#coding: utf-8
from math import sin, cos, pi, sqrt, acos
import sys
import pygame

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((1200, 800))
FPS = 60
black = pygame.Color(0, 0, 0)

pos_1 = (600, 400)
radius_1 = 20
c_red = pygame.Color(255, 0, 0)

alpha = 0
h = 100
radius_2 = 5
c_green = pygame.Color(0, 255, 0)
omega = 2*pi/600. # 0.1 Hz

pos_3 = (200, 200)
radius_3 = 5
c_blue = pygame.Color(0, 0, 255)
v_3 = (1, 1)
h_3 = 150
G = 1000
alpha_3 = None
omega_3 = -2*pi/600. # 0.1 Hz

while True:
    screen.fill(black)
    planet_1 = pygame.draw.circle(screen, c_red, pos_1, radius_1)
    pygame.event.pump() # FIXME cheatsheet does this differently
    keys = pygame.key.get_pressed()
    if keys[pygame.K_ESCAPE]:
        sys.exit()
    
    alpha += omega
    pos_2 = (round(pos_1[0]+(h*cos(alpha))), round(pos_1[1]+(h*sin(alpha))))
    planet_2 = pygame.draw.circle(screen, c_green, map(int, pos_2), radius_2)
    
    g_3 = (pos_1[0]-pos_3[0], pos_1[1]-pos_3[1])
    r_3 = sqrt(g_3[0]**2 + g_3[1]**2)
    if (r_3 <= h_3 + 1.0):
        if alpha_3 is None:
            alpha_3 = acos((pos_3[0]-pos_1[0])/r_3)
            if pos_3[1] > pos_1[1]:
                alpha_3 += pi
            print alpha_3
        else:
            alpha_3 += omega_3
        pos_3 = (round(pos_1[0]+(h_3*cos(alpha_3))), round(pos_1[1]+(h_3*sin(alpha_3))))
    else:
        alpha_3 = None
        a_3 = G/r_3**2
        #print a_3
        v_3 = (v_3[0] + a_3*g_3[0]/r_3, v_3[1] + a_3*g_3[1]/r_3)
        pos_3 = (pos_3[0] + v_3[0], pos_3[1] + v_3[1])
    planet_3 = pygame.draw.circle(screen, c_blue, map(int, pos_3), radius_3)

    pygame.display.update() # FIXME no galaxians .flip()
    clock.tick(FPS)
    

