# coding: utf-8
import os.path
import pygame
from pygame.locals import *
from globalvars import *


# Astronaut
class Astronaut(object):
    # TODO class sprite anim
    VX = 2
    VY = -3
    VJUMP = -5
    AFLY = -0.5
    W = 29
    H = 37
    HOP_THRESHOLD = 5
    
    def __init__(self, r):
        # convert_alpha keeps the transparency from the original!
        self._image = load_image('astronaut3_0.png').convert_alpha()
        self.jetpac_sound = load_sound('Powerup3.wav')
        self._framepos = ( [(self.W*i, 0, self.W, self.H) for i in range(3)], # L->R
                           [(self.W*i, self.H, self.W, self.H) for i in range(3)]) # R->L
        self._framecounter = 0 # FIXME usar cycle iterator
        self._area = self._framepos[0][0]
        self._facing = RIGHT
        self.rect = pygame.Rect(r)
        self.old_rect = None
        self.v = {'x': 0, 'y': 0} 
        self.old_v = None
        self.moving = False
        self.step_counter = 0
        self.flying = False
        self.update_sprite = False
        self.skid = False
        self.dead = False
        self.buried = False
        self.has_jetpac = False
        
    def draw(self, screen):
        #pygame.draw.rect(screen, self.colour, self.rect)
        screen.blit(self._image, self.rect, self._area)
        
    def jump(self, side):
        if self.moving or self.flying:
            return
        if side is not None:
            self.hop(side)
        self.v['y'] = self.VJUMP
        self.moving = True
        self.update_sprite = True
    
    def move(self, side):
        if self.flying:
            if side == LEFT:
                self.v['x'] += -0.1
                if self._facing == RIGHT:
                    self.update_sprite = True
                self._facing = LEFT
            elif side == RIGHT:
                self.v['x'] += 0.1
                if self._facing == LEFT:
                    self.update_sprite = True
                self._facing = RIGHT
            return
        if self.moving or self.flying:
            return
        if self.step_counter >= self.HOP_THRESHOLD:
            self.hop(side)
            self.update_sprite = True
        else:
            if side == LEFT:
                self.v['x'] = -1
                self._facing = LEFT
            elif side == RIGHT:
                self.v['x'] = 1
                self._facing = RIGHT
            self.step_counter += 1
            self.update_sprite = True
        self.moving = True
    
    def hop(self, side):
        if side == LEFT:
            sign = -1
            self._facing = LEFT
        elif side == RIGHT:
            sign = 1
            self._facing = RIGHT
        self.v['x'] = sign*self.VX
        self.v['y'] = self.VY
        self.skid = False

    def update(self):
        if self.dead:
            if self._area[3] > 0:
                new_h = self._area[3] - 1
                self._area = (self._area[0], self._area[1], self._area[2], new_h)
            else:
                self.buried = True
            return
        self.apply_gravity()
        self.old_rect = self.rect
        if self.skid:
            self.skid = False
            if self.old_v['x'] < 0:
                self.v['x'] = -2
            elif self.old_v['x'] < 0:
                self.v['x'] = 2
            self.moving = True
        if not (self.moving or self.flying):
            self.v['x'] = 0
        if (abs(self.v['x']) >= 1) or (abs(self.v['y']) >= 1):
            self.rect = self.rect.move(self.v['x'], self.v['y'])
            if self.update_sprite:
                self._framecounter += 1
                d = 1 if self._facing == LEFT else 0
                frame = int(self._framecounter) % len(self._framepos[d])
                self._framecounter = frame
                self._area = self._framepos[d][frame]
                self.update_sprite = False
                
    def idle(self):
        if not self.moving:
            self.step_counter = 0
            
    def fly(self):
        # enter fly mode if we are equipped with the JetPac
        if self.has_jetpac:
            self.flying = True
            self.moving = False
            self.v['y'] += self.AFLY

    def land(self, on):
        self.v['y'] = 0
        self.rect.bottom = on.top
        if self.old_v['y'] >= 1:
            self.skid = True
        self.moving = False
        self.flying = False

    def apply_gravity(self):
        self.v['y'] += ay
        
    def backup_v(self):
        self.old_v = dict(**self.v)

    def die(self):
        self.dead = True
        
    def collide(self, obstacle):
        return self.rect.colliderect(obstacle.rect)

    def equip(self):
        self.has_jetpac = True
        self.jetpac_sound.play()
