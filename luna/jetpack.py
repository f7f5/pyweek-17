# coding: utf-8
import os.path
import pygame
from globalvars import *

class JetPack(object):
    W = 43
    H = 30

    def __init__(self, r):
        self.rect = pygame.Rect((r[0], r[1], self.W, self.H))
        self.image = load_image('individual_jet_pack.png').convert_alpha()

    def draw(self, surface):
        surface.blit(self.image, self.rect)
