# coding: utf-8
from itertools import chain
import os.path
import pygame
from globalvars import *

ground_texture = load_image('road2.JPG').convert()
rock_texture = load_image('rock.png').convert_alpha()


class Rock(object):
    W = 46
    H = 40

    def __init__(self, r):
        self.rect = pygame.Rect((r[0], r[1], self.W, self.H))
        
    def draw(self, surface):
        surface.blit(rock_texture, self.rect)


class GameScreen(object):
    def __init__(self, scrdef):
        self.surface = pygame.Surface((800, 600)).convert()
        self.ground = map(pygame.Rect, scrdef['ground'])
        self.rocks = [Rock(r) for r in scrdef['rocks']]

        for g in self.ground:
            #r = pygame.draw.rect(ground_surface, moon_colour, g)
            self.surface.blit(ground_texture, g, g)
        for r in self.rocks:
            r.draw(self.surface)
            
    @property
    def obstacles(self):
        return list(chain(self.ground, [r.rect for r in self.rocks]))

    def collide(self, a):
        return [self.obstacles[g] for g in a.rect.collidelistall(self.obstacles)]
