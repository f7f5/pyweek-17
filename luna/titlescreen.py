# coding: utf-8
import os.path
import sys
import pygame
from globalvars import *
from pygame.locals import FULLSCREEN

def show_title_screen(screen):
    global screen_flags
    bgimage = load_image('bg.png').convert()
    load_music('Beethoven_Moonlight_1st_movement.ogg')
    screen.fill(black)
    screen.blit(bgimage, (0, 0))
    pygame.display.update()
    pygame.mixer.music.play()
    
    while True:
        event = pygame.event.wait()
        etype = event.type
        if etype is pygame.KEYDOWN:
            k = event.key
            if k in [pygame.K_ESCAPE, pygame.K_q]:
                sys.exit()
            elif k == pygame.K_F10:
                screen_flags = screen_flags^FULLSCREEN
                screen = pygame.display.set_mode((800, 600), screen_flags)
                screen.blit(bgimage, (0, 0))
                pygame.display.update()
            else:
                pygame.mixer.music.stop()
                return
        elif etype is pygame.QUIT:
            sys.exit()

def render_text(screen, txt, font_size, y):
    if pygame.font:
        font = pygame.font.Font(None, font_size)
        for line in txt.split('\n'):
            text = font.render(line, 1, (255, 255, 10))
            textpos = text.get_rect(centerx=400)
            textpos.y = y
            screen.blit(text, textpos)
            y += font_size
            
def show_end_screen(screen):
    load_music('Beethoven_Moonlight_1st_movement.ogg')
    bgimage = load_image('endscreen.png').convert()
    screen.fill(black)
    screen.blit(bgimage, (0, 0))
    pygame.display.update()
    pygame.mixer.music.play()
    
    while True:
        event = pygame.event.wait()
        etype = event.type
        if etype is pygame.KEYDOWN:
            k = event.key
            if k in [pygame.K_ESCAPE, pygame.K_q]:
                sys.exit()
        elif etype is pygame.QUIT:
            sys.exit()
