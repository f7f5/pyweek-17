# coding: utf-8
import os.path
import pygame

v = 3
LEFT, RIGHT = 0, 1
vy = -4
ay = 0.2

black = pygame.Color(0, 0, 0)
gray = pygame.Color(100, 100, 100)
blue = pygame.Color(0, 0, 255)
moon_colour = pygame.Color("#4f4e76")

screen_flags = 0

def load_sound(name):
    filename = os.path.join('data', 'audio', name)
    class NoneSound:
        def play(self): pass
    if not pygame.mixer:
        return NoneSound()
    try:
        sound = pygame.mixer.Sound(filename)
    except pygame.error, message:
        print 'Cannot load sound:', filename
        raise SystemExit, message
    return sound

def load_image(name):
    filename = os.path.join('data', 'art', name)
    return pygame.image.load(filename)

def load_music(name):
    filename = os.path.join('data', 'audio', name)
    pygame.mixer.music.load(filename)
