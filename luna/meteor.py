# coding: utf-8
from random import randint
import pygame
from pygame.locals import *
from globalvars import *


class Meteor(object):
    W = 60
    H = 60

    def __init__(self, r, vx=1):
        self.v = {'x': vx, 'y': 4}
        self.rect = pygame.Rect(r)
        self._image = load_image('animated_asteroid2_0.png').convert_alpha()
        self._framepos = [(self.W*i, 0, self.W, self.H) for i in range(16)] # L->R
        self._framecounter = 0 # FIXME usar cycle iterator
        self._area = self._framepos[0]
    
    def draw(self, screen):
        screen.blit(self._image, self.rect, self._area)

    def update(self):
        self.rect.left += self.v['x']
        self.rect.top += self.v['y']
        self._framecounter += 1
        self._framecounter = self._framecounter % len(self._framepos)
        self._area = self._framepos[self._framecounter]

        
class MeteorExplosion(object):
    W = 128
    H = 128

    def __init__(self, r):
        self.rect = pygame.Rect(r)
        self._image = load_image('explosion.png').convert_alpha()
        self._framepos = [(self.W*i, 0, self.W, self.H) for i in range(17)] # L->R
        self._framecounter = 0
        self._area = self._framepos[0]
        self.sound = load_sound('Explosion.wav')
        self.sound.play()
    
    def draw(self, screen):
        screen.blit(self._image, self.rect, self._area)

    def update(self):
        self._framecounter += 0.5
        if int(self._framecounter) >= len(self._framepos):
            return False
        self._area = self._framepos[int(self._framecounter)]
        return True


def meteor_generator():
    return Meteor((randint(0, 800 - Meteor.W), 0, Meteor.W, Meteor.H),
        randint(-5, 5) or 1)
