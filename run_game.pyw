#coding: utf-8
"""
"""
import sys
import pygame
from pygame.locals import *
from luna.globalvars import *

# init Pygame
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((800, 600), screen_flags)
pygame.display.set_caption('Luna')

from luna.astronaut import Astronaut
from luna.gamescreen import GameScreen
from luna.meteor import Meteor, MeteorExplosion, meteor_generator
from luna.titlescreen import show_title_screen, show_end_screen
from luna.jetpack import JetPack

pygame.mouse.set_visible(0)
FPS = 60



def collide(moving_ob, obstacle):
    obst_rect = obstacle if isinstance(obstacle, pygame.Rect) else obstacle.rect
    # left to right
    if moving_ob.old_rect.right <= obst_rect.left and moving_ob.rect.right > obst_rect.left:
        moving_ob.v['x'] = 0
        if moving_ob.v['y'] < 0 :
            moving_ob.v['y'] = 0
        moving_ob.rect.right = obst_rect.left - 1
    # right to left
    if moving_ob.old_rect.left >= obst_rect.right and moving_ob.rect.left < obst_rect.right:
        moving_ob.v['x'] = 0
        if moving_ob.v['y'] < 0 :
            moving_ob.v['y'] = 0
        moving_ob.rect.left = obst_rect.right + 1
    # fall on object
    if (moving_ob.old_rect.bottom <= obst_rect.top) and moving_ob.rect.bottom > obst_rect.top:
        moving_ob.land(obst_rect)
    # Bang your head
    if (moving_ob.old_rect.top >= obst_rect.bottom) and moving_ob.rect.top < obst_rect.bottom:
        moving_ob.v['y'] = 0
        moving_ob.rect.top = obst_rect.bottom + 1


screen_defs = [ 
                { 'ground': [(0, 500, 800, 100)], 'rocks': []},
                { 'ground': [(0, 500, 800, 100)], 'rocks': [(300, 460)]},
                { 'ground': [(0, 500, 438, 100), (500, 500, 300, 100),
                             (300, 460, 138, 40), (500, 460, 138, 40),
                             (346, 420, 92, 40), (500, 420, 92, 40)], 
                  'rocks': [(256, 460), (638, 460),
                            (300, 420), (592, 420)]},
                { 'ground': [(0, 500, 800, 100)], 'rocks': [], 'meteors': 1},
                { 'ground': [(0, 500, 800, 100)], 'rocks': [], 'meteors': 3},
                { 'ground': [(0, 500, 800, 100)], 'rocks': [(550, 460), (550, 420), (550, 380), (550, 340), (596, 460), (596, 420), (596, 380), (642, 460), (642, 420), (688, 460)], 'jetpack': (200, 470)},
                { 'ground': [(0, 500, 438, 100), (500, 500, 300, 100),
                             (300, 460, 138, 40), (500, 460, 138, 40),
                             (346, 420, 92, 40), (500, 420, 92, 40)], 
                  'meteors': 4,
                  'rocks': [(256, 460), (638, 460),
                            (300, 420), (592, 420), (200, 460)]},
                { 'ground': [(0, 500, 280, 100), (200, 200, 80, 300), 
                             (200, 0, 450, 100), (350, 410, 70, 400), 
                             (350, 100, 70, 250), (500, 280, 100, 320),
                             (500, 100, 100, 110), (650, 0, 50, 420),
                             (650, 500, 150, 100),
                            ], 
                  'rocks': []
                             }
            ]
game_screens = [GameScreen(g) for g in screen_defs]

start_h = 500
astronaut = Astronaut((0, start_h-Astronaut.H, Astronaut.W, Astronaut.H))

def change_screen(advance=True):
    # Change screen
    global current_screen, ground_surface, explosions, meteors, screen, powerups
    if advance:
        current_screen += 1
        if current_screen >= len(game_screens):
            show_end_screen(screen)
    ground_surface = game_screens[current_screen].surface
    astronaut.rect.left = 0
    astronaut.rect.bottom = start_h
    
    meteors = []
    for i in range(screen_defs[current_screen].get('meteors', 0)):
        meteors.append(meteor_generator())
    explosions = []
    
    powerups = []
    j = screen_defs[current_screen].get('jetpack', None)
    if j:
        powerups.append(JetPack(j))
    # 
    screen.fill(black)
    screen.blit(ground_surface, (0, 0))
    pygame.display.update()


show_title_screen(screen)

current_screen = 0
explosions = []
meteors = []
change_screen(advance=False)
game_stopped = False


#frames = 0
#dt = 0
stats = {
    'events': 0,
    'update': 0,
    'redraw': 0,
    'redraw_astronaut': 0,
    'redraw_explosions': 0,
    'redraw_meteors': 0,
    'redraw_screen': 0
    }
while True:
    #t0 = pygame.time.get_ticks()
    dirty_list = []
    pygame.event.pump()
    keys = pygame.key.get_pressed()
    if keys[pygame.K_ESCAPE]:
        pygame.quit()
        sys.exit()
    if not game_stopped:
        if keys[pygame.K_F10]:
            screen_flags = screen_flags^FULLSCREEN
            screen = pygame.display.set_mode((800, 600), screen_flags)
            dirty_list.append((0,0,800,600))
        if keys[pygame.K_RSHIFT] or keys[pygame.K_LSHIFT]:
            astronaut.fly()
        if keys[pygame.K_LEFT]:
            side = LEFT
        elif keys[pygame.K_RIGHT]:
            side = RIGHT
        else:
            side = None
        if keys[pygame.K_UP]:
            astronaut.jump(side)
        elif side is not None:
            astronaut.move(side)
        else:
            astronaut.idle()
    elif astronaut.buried:
        jp = astronaut.has_jetpac
        astronaut = Astronaut((0, start_h-Astronaut.H, Astronaut.W, Astronaut.H))
        astronaut.has_jetpac = jp
        change_screen(advance=False)
        game_stopped = False

    #t1 = pygame.time.get_ticks()
    #stats['events'] += t1 - t0
    #t0 = pygame.time.get_ticks()
    screen.fill(black)
    

    # Astronaut
    dirty_list.append(astronaut.rect.copy())
    astronaut.update()
    for g in game_screens[current_screen].collide(astronaut):
        collide(astronaut, g)
    if astronaut.rect.left < 0:
        astronaut.rect.left = 0
    if astronaut.rect.right > 800:
        change_screen()
    if astronaut.rect.top < 0:
        astronaut.rect.top = 0
        astronaut.v['y'] = 0
    if astronaut.rect.bottom > 600:
        game_stopped = True
        astronaut.die()
        
    if not game_stopped:
        astronaut.backup_v()
        
        for x in explosions:
            dirty_list.append(x.rect.copy())
            if not x.update():
                explosions.remove(x)
                meteors.append(meteor_generator())
            
        # Meteors
        for met in meteors:
            dirty_list.append(met.rect.copy())
            met.update()
            if game_screens[current_screen].collide(met):
                explosions.append(MeteorExplosion(met.rect.inflate(60, 60)))
                meteors.remove(met)
            elif met.rect.left < 0 or met.rect.right > 800:
                meteors.remove(met)
                meteors.append(meteor_generator())
            elif astronaut.collide(met):
                game_stopped = True
                astronaut.die()
        
        # Powerups
        for pw in powerups:
            if astronaut.collide(pw):
                dirty_list.append(pw.rect)
                powerups.remove(pw)
                astronaut.equip() 
                        
    #t1 = pygame.time.get_ticks()
    #stats['update'] += t1 - t0
    #t0 = pygame.time.get_ticks()
    # Draw all
    screen.blit(ground_surface, (0, 0))
    #t1 = pygame.time.get_ticks()
    #stats['redraw_screen'] += t1 - t0
    #t0 = pygame.time.get_ticks()
    astronaut.draw(screen)
    dirty_list.append(astronaut.rect)
    #t1 = pygame.time.get_ticks()
    #stats['redraw_astronaut'] += t1 - t0
    #t0 = pygame.time.get_ticks()
    for met in meteors:
        met.draw(screen)
        dirty_list.append(met.rect)
    #t1 = pygame.time.get_ticks()
    #stats['redraw_meteors'] += t1 - t0
    #t0 = pygame.time.get_ticks()    
    for ex in explosions:
        ex.draw(screen)
        dirty_list.append(ex.rect)
    #t1 = pygame.time.get_ticks()
    #stats['redraw_explosions'] += t1 - t0
    for pw in powerups:
        pw.draw(screen)
        dirty_list.append(pw.rect.copy())

    #t0 = pygame.time.get_ticks()
    
    pygame.display.update(dirty_list)
    #t1 = pygame.time.get_ticks()
    #stats['redraw'] += t1 - t0
    #dt += clock.tick(FPS)
    clock.tick(FPS)
    # MYTICK
    #frame_t = 0
    #while frame_t < 16:
        #frame_t += clock.tick()
    #dt += frame_t
    #dt += clock.tick_busy_loop(FPS)
    #frames += 1
    #ms = int(s*1000)
    #if (frames == 60):
        #print "Fps: %f" % clock.get_fps()
        #print "Fps2: %d, %d, %f" % (frames, dt, frames/(float(dt)/1000.))
        #print stats['redraw']/float(frames)
        #stats['redraw'] = 0
        #frames = 0
        #dt = 0
