====
Luna
====


Entry in PyWeek #17  <http://pyweek.org/17/>
URL: http://pyweek.org/e/luna
By: f7f5 <f7f5@myopera.com>
License: MIT (see LICENSE.txt) except where otherwise noted (see the Credits section below)


Running the Game
================

This game requires python 2 (tested on 2.7) and Pygame (tested on 1.9.1release).

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


How to Play the Game
====================

Guide the astronaut back to the base.


Controls
--------

Left and right arrow keys - move left/right
Up arrow key - jump
Shift - use Jetpack

ESC - Quit
F10 - Toggle fullscreen


Credits
=======

Designed and coded by f7f5 <f7f5@myopera.com>.

This game uses several freely available assets created by 3rd parties.

Graphics:
astronaut sprites by Pilla Julien, CC-BY 3.0 [http://creativecommons.org/licenses/by/3.0/] http://opengameart.org/content/astronaut-0

Rocks by "Écrivain", public domain CC-0 [http://creativecommons.org/publicdomain/zero/1.0/]
http://opengameart.org/content/rocks

Asteroid by "nightzero", CC-BY 3.0 [http://creativecommons.org/licenses/by/3.0/]
http://opengameart.org/content/2d-asteroid-sprite

Explosion animation from JS WARS by Jonas Wagner - http://29a.ch/: http://opengameart.org/content/asteroid-explosions-rocket-mine-and-laser
CC-BY 3.0 [http://creativecommons.org/licenses/by/3.0/]

Jetpack, Public Domain CC-0 [http://creativecommons.org/publicdomain/zero/1.0/]
http://www.clker.com/clipart-25167.html

Background pictures used in the title and end screens by NASA
http://www.apolloarchive.com

Music:
Beethoven: Moonlight sonata 1st movement. Author: Bernd Krueger
http://en.wikipedia.org/wiki/File:Beethoven_Moonlight_1st_movement.ogg
CC-BY-SA 2.0 Germany [http://creativecommons.org/licenses/by-sa/2.0/de/deed.en]

Extra sounds created with Bfxr [http://www.bfxr.net/] and Audacity.
Gimp was used for graphics editing.
